
zero,68,203,5	script	Mapper Girl	101,{	

	begin( strML("Maníaca dos Mapas", "Mapper Girl" ),
		   0,
		   "Olá",
		   "Hello!" );
		   
	nextButton2();

	if( isbegin_quest(1044) == 1 )
		callfunc "turjob";
	
	if( countitem( 7111 ) >= 25 )
	{
		show( "Uau, você tem um material de qualidade aí com você. Eu posso fazer um mapa com esses papéis?",
			  "Wow, you have quality stuff right there with you. Can I make a map with these papers?" );
		
		addButton( 1, "Sim", "Yes", BT_SPECIAL );
		addButton( 2, "Não", "No",  BT_NORMAL  );
		waitButton();
		
		if( BUTTON == 1 )
		{
			getitem 6447,1;
			delitem 7111,25;
		}
		else
			show( "Okay!" );
		
		endTalk();
	}
	
	show( "Eu amo desenhar mapas. Mapas são vida, mapas são amor.",
	      "I love drawing maps. Maps are life, maps are love." );
	nextButton2();
	
	show( "Com meus mapas, posso viajar o mundo e nunca me perder!",
	      "With my maps, I can travel the world and never get lost!" );
		  
	addButton( 1, "Posso ver?", "Can I have one?", BT_SPECIAL );
	addButton( 2, "Ok...",      "Ok...",           BT_NORMAL  ); 
	waitOption();
	clear();
	
	switch( BUTTON )
	{
		case 1:
			show( "Então você também é um amante dos mapas?!",
			      "So you're a map lover too?!" );
			nextButton2();
			
			show( "Cardinalidade... pontos cartesianos, coordenadas, acho que vou ter um orgasmo!!",
			      "Cardinality ... Cartesian points, coordinates, I think I'll have an orgasm!!" );
			nextButton2();
			
			show( "Desculpa, acabei me perdendo em meus pensamentos impuros sobre mapas.",
			      "Sorry, I got lost in my impure thoughts on maps." );
			nextButton2();
			
			show( "Bem, para fazer um mapa com qualidade digna eu preciso de 25 folhas de papéis.",
			      "Well, to make a map with decent quality I need 25 sheets of paper." );
			nextButton2();
			
			show( "Traga para mim e eu farei um mapa para você.",
			      "Bring it to me and I'll make you a map." );
			break;
			
		case 2:
			show( "HIHIHIHIHI MAPAAAAAAAAAAAAAAS",
			      "HEHEHEHEHE MAAAAAAPS" );
			break;
	}
	endTalk();
	
	OnQuest:
		showevent 2,1;
		end;
	
	OnInit:
		SLN( PTBR, "Maníaca dos Mapas" );
		end;
}

prt_in,285,167,3	script	Gerente do Clã de Assis::Spear	571,{

	.@faith_lancer = mercenary_get_faith(SPEAR_MERC_GUILD);
	
	begin( strML( "Gerente de Assistentes", "Assistant manager" ),
	       0,
		   "Bem-vindo à Guilda dos Assistentes Lanceiros, em que posso ajudar?",
		   "Welcome to the Guild of Assistants Lancers, how I can help you?" );

	addOption( 1, "Contratar Assistente",           "Hire an Assistant",          BT_SPECIAL );
	addOption( 2, "Informações sobre Assistentes!", "Information of Assistants!", BT_NORMAL  );
	addOption( 3, "Nada",                           "Nothing",                    BT_NORMAL  );
	addOption( 4, "Assistentes de 10º Nível",       "Level 10 Assistants",        BT_SPECIAL );
	waitOption();
	clear();
	
	switch( BUTTON )
	{
		case 1:
			show( "Você deseja contratar um Assistente Lanceiro?",        "Do you want to hire a lancer assistant?"      );
			show( "Qual Nível de Assistente você gostaria de contratar?", "What level of Assistant do you want to hire?" );

			.@faith[7] = 50;
			.@faith[8] = 100;
			.@faith[9] = 300;
			
			for( .@i = 1; .@i <= 9; .@i++ )
		 		addOption( .@i, "Assistente Lançeiro "+.@i+"º Nível", "Lancer Assistant "+.@i+"º Level" , BT_NORMAL );
		
			waitOption();
			clear();
			
			.@BaseLevel = 5 + ( BUTTON * 10 );
			
			if( .@BaseLevel > 90 )
				.@BaseLevel = 90;
				
			.@ZenyCost  = 10 * BUTTON;
			.@FaithCost = .@faith[BUTTON];
			
			.@ASN = BUTTON;
			
			show( "Então você quer contratar um Assistente Lanceiro de " + BUTTON + "º Nível?",,
			      "So do you want to hire an Lancer Assistant of "+ .@ASN +"º Level?" );
			nextButton2();	
			
			show( "Você precisa ter Nível de Base " + .@BaseLevel + " ou maior, além de pagar a taxa de serviço de " + .@ZenyCost + ",000 zenys.",
			      "You need to have base level "+ .@BaseLevel + " or higher and pay a service fee of"+ .@ZenyCost +",000 zenys." );
				  
			addOption( 1, "Sim", "Yes", BT_SPECIAL );
			addOption( 2, "Não", "No",  BT_NORMAL  );
			waitOption();	
			clear();
			
			if( BUTTON == 2 )
			{
				show( "Oh, é mesmo? Bem, agora talvez não seja uma boa hora para você pensar em contratar um Assistente, mas sinta-se à vontade para voltar caso mude de idéia. Obrigado.",
				      "Oh, is this? Right, maybe now is not a good timing for you think in hire an Assistant,but feel free to come back when you change your mind. Thanks.");
			}		
			else if( .@FaithCost && .@faith_lancer < .@FaithCost )
			{			
				show( "Sinto muito, mas sua taxa de Lealdade é muito baixa para contratar um Assistente de " + .@ASN + "º Nível",
				      "I am sorry, but your Loyalty rate is so low for hire an Assistant of "+ .@ASN + "º Level" );
				nextButton2();
				
				show( "Você deve ter "+ .@FaithCost+" ou mais pontos de Lealdade para fazer um contrato com um deles.",
				      ". You need to have "+ .@FaithCost+" or more points of Loyalty to hire one of them. );
			}
			else if( BaseLevel < .@BaseLevel )
			{
				show( "Sinto muito, mas seu Nível de Base não é alto o suficiente para contratar este Assistente.",
			      	  "I am sorry, but your base level isn't high enough to hire this Assistant. ." );
				nextButton2();
				
				show( "Por favor, volte "assim que tiver atingido o Nível de Base " + .@BaseLevel + ".",
				      "Please come back when you get at least base level "+ .@BaseLevel +"." );
			}
			else if( Zeny < .@ZenyCost * 1000 )
			{
				show( "Sinto muito, mas você não tem zeny para contratar este Assistente",
				      "I am sorry, but you don't have enough zeny to hire this assistant" );
				nextButton2();
				
				show( ". A taxa de contratação é de " + .@ZenyCost + ",000 zenys.",". The hire fee is " + .@ZenyCost + ",000 zenys.");
			}
			else
			{
				show( "Ótimo! Nossos Assistentes são honestos e devotadosà proteção de nossos clientes.",
				      "Good! Our Assistants are honest and devoted to protect our customers." );
				nextButton2();
				
				show( "Os Assistentes convocados oferecerão seus serviços a você por 30 minutos. Cuide-se.",
				      "The summoned assistants "will offer their works to you for 30 minutes. Take care.");
				closeTalk();
				Zeny -= .@ZenyCost * 1000;
				getitem 12172 + .@ASN, 1;
				finish();
			}
			endTalk();
		
		case 2:
			show( "Assistentes são soldados que lutarão a seu lado no campo de batalham, mas existe alguns termos e condições que você deve cumprir para contratá-los.",
			      "Assistants are soldiers that will fight at your side on the battlefield, but there are some terms and requirements need for hire them." );
			nextButton2();

			show( "Você deve cumprir o requisito de nível e pagar uma taxa em zeny para contratar um Assistente. Assistente de alto nível também tem como condição que você tenha um certo nível de Lealdade a nós.",
			      "You need to fill the requirement and pay a zeny tax for hire an Assistant. High level assistants also has a requirement of having a certain loyalty with us.");
			nextButton2();

			show( "Contratos de Assistentes não podem ser transferidos para outras pessoas, e nós só permitimos uma diferença de 5 Níveis de Base entre Assistente e cliente, então você não pode contratar alguém muito mais forte que você.",
			      "Assistant contracts can't be transferred to other people and we only permit a 5 base level difference between the assistant and the customer, so you won't hire someone much more stronger than you." );
			nextButton2();

			show( "Bem, você pode se informar melhor dos detalhes quando você de fato contratar um de nossos Assistentes e receber o Pergaminho de Invocação, que irá convocar um Assistente para lhe escoltar.",
			      "You can have a more detailed information when you hire one of our assistants and receive a Summon Scroll, that will summon the Assistant to escort you.");
			nextButton2();

			show( "Você não pode dar esse pergaminho para outra pessoa e o Assistente só ficará com você até 30 minutos depois que você convocá-lo. Não esqueça do limite de tempo, ok?",
			      "You can't give this scroll to another person and the Assistant will only remain at your side for 30 minutes after you summon him. Don't forget the time limit, ok?" );
			endTalk();
		
		case 3:
			show( "Não? Você não precisa de nenhuma ajuda? Bem, fique à vontade para perguntar o que quiser saber sobre Assistentes.",
			      "No? You don't need any help? Right, feel free to ask anything about the Assistants." );
			endTalk();

		case 4:
			show( "Assistentes de 10º Nível são os melhores que temos,por isso usamos outros critérios para nossos clientes contratá-los. Não há taxa de zeny, mas você deve ter 500 pontos de Lealdade à Guilda.",
			      "Assistant of 10º level are the best that we have, because of this we use other criteria for our customers being enable to hire them. There is no zeny tax, but you need to have 500 points of loyalty with this guild.");
			nextButton2();
		
			show( "Ao fazer um contrato com um Assistente de 10º Nível, sua taxa de Lealdade será reduzida em 400 pontos. Ou seja, você paga 400 pontos de Lealdade para contratar um Assistente de 10º Nível.",
			      "When you make a contract with a 10º level Assistant, your loyalty rate will be reduced in 400 points. In other words, you will pay 400 points of loyalty to hire an Assistant of 10º level.");
			nextButton2();
		
			show( "Você também deve ter Nível Base 90 ou maior para contratar um Assistente de 10º Nível.",
			      "You also need to have a base level greater than 90 to hire an Assistant of 10º level." );
			nextButton2();
		
			.@ex$ = strML( "Entendo... É necessário muito esforço e sacrifício para alcançar os requisitos de contrato de um Assistente de 10º Nível. Você já pensou em	contratar um Assistente de nível menor?",
						   "I see...It's needed so much effort and sacrifice to reach on requeriments for hire a 10º grade assistant. Did you thought about hiring a lower Assistant?" );
						   
			show( "Você ainda está interessado em fazer este contrato?",
			      "Are you still interested in make this contract?" );
		
			addButton( 1, "Sim", "Yes", BT_SPECIAL           );
			addButton( 2, "Não", "No",  BT_NORMAL, 0, .@ex$ );
			waitButton();
			clear();
			
			if( BUTTON == 2 )
				show( .@extra_str$[BUTTON] );
			else if( .@faith_lancer < 500 )
				show("Sinto muito, mas sua taxa de Lealdade é muito baixa para contratar um Assistente deste nível.","I am sorry, but your Loyalty rate is so low for hire an Assistant of this level.");
			else if( BaseLevel < 90 )		
				show("Sinto muito, mas seu Nível de Base não é alto o suficiente.","I am sorry, but your base level isn't high enough.");
			else
			{
				show( "Ótimo! Nossos Assistentes são honestos e devotadosà proteção de nossos clientes.",
				      "Good! Our Assistants are honest and devoted to protect our customers." );
				nextButton2();
				
				show( "Os Assistentes convocados oferecerão seus serviços a você por 30 minutos. Cuide-se.",
				      "The summoned assistants "will offer their works to you for 30 minutes. Take care.");
					  
				mercenary_set_faith SPEAR_MERC_GUILD,-400;
				getitem 12182,1;
			}
			endTalk();
	}
	end;
	
	OnInit:
		SLN( PTBR, "Gerente de Assistentes" );
		end;
	
}

prt_in,277,172,3	script	Assistente#zero	708,{

	begin( strML( "Assistente", "Assistant" ),
	        getnpcid(0),
		    "Para utilizar melhor nossos serviços, digite /merai quando estiver com um de nossos Assistentes.",
		    "To get the best from our services, type /merai when you are with one of our Assistants" );	
	endTalk();
	
	OnInit:
		SLN( PTBR, "Assistente" );
		end;
}
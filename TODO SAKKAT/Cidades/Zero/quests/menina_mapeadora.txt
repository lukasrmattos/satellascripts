
zero,68,203,5	script	Girl Mapper	101,{	

	.@n$ = strML( "(^4682B4Man�aca dos Mapas^000000)", "(^4682B4Girl Mapper^000000)";
	
	if(isbegin_quest(1044)==1) callfunc "turjob";
	
	mes .@n$;
	
	if( countitem( 7111 ) >= 25 )
	{
		mesl "Uau, voc� tem um material de qualidade a� com voc�. Eu posso fazer um mapa com esses pap�is?",
			 "Wow, you have quality stuff right there with you. Can I make a map with these papers?";
			 
		.@m$ = strML( "- Sim:- N�o", "- Yes:- No" );
		
		if( select( .@m$ ) == 1 )
		{
			getitem 6447,1;
			delitem 7111,25;
		}
		
		close;
	}
	
	mesl "Eu amo desenhar mapas. Mapas s�o vida, mapas s�o amor.",
		 "I love drawing maps. Maps are life, maps are love.";
	next;
	mes .@n$;
	mesl "Com meus mapas, posso viajar o mundo e nunca me perder!",
		 "With my maps, I can travel the world and never get lost!";
		 
	.@m$ = strML( "- Voc� pode me dar um?:- Ok...", "- Can I have one?:- Ok ..." );
	
	switch( select( .@m$ ) )
	{
		case 1:
			next;
			mes .@n$;
			mesl "Ent�o voc� tamb�m � um amante dos mapas?!",
				 "So you're a map lover too?!";
			next;
			mes .@n$;
			mesl "Cardinalidade... pontos cartesianos, coordenadas, acho que vou ter um orgasmo!!",
				 "Cardinality ... Cartesian points, coordinates, I think I'll have an orgasm!!";
			next;
			mes .@n$;
			mesl "Desculpa, acabei me perdendo em meus pensamentos impuros sobre mapas.",
				 "Sorry, I got lost in my impure thoughts on maps.";
			next;
			mes .@n$;
			mesl "Bem, para fazer um mapa com qualidade digna eu preciso de 25 folhas de pap�is.",
				"Well, to make a map with decent quality I need 25 sheets of paper.";
			next;
			mes .@n$;
			mesl "Traga para mim e eu farei um mapa para voc�.",
				 "Bring it to me and I'll make you a map.";
			close;
			
		case 2:
			next;
			mes .@n$;
			mesl "HIHIHIHIHI MAPAAAAAAAAAAAAAAS",
				 "HEHEHEHEHE MAAAAAAPS";
			close;
	}
	
	OnQuest:
	showevent 2,1;
	end;
	
	OnInit:
		SLN( $E_PTBR, "Man�aca dos Mapas" );
		end;
}

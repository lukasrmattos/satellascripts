
payon,246,154,0	script	Young Lady#payon	90,{

	begin( strML( "Jovem Senhora", "Young Lady" ),
		   0,
		   "De volta � hist�ria. Passamos por momentos de muito aperto nessa vila de " + $S_CIDADEPAYON$ + ".",
		   "Back to history. We went through times of great tightness in this village of " + $S_CIDADEPAYON$ + "." );	   
	nextButton2();
	
	show( "As pessoas viviam na mis�ria, ent�o eles n�o podiam pagar a morte para descansar em paz.",
		  "People lived in misery, so they could not pay death to rest in peace." );
	nextButton2();
	
	show( "Algumas pessoas jogavam seus familiares mortos na caverna perto da vila.",
		  "Some people threw their dead relatives into the cave near the village." );
	nextButton2();
	
	show( "Por isso, essa caverna ganhou fama de ter muitos zumbis que nunca puderam partir para o al�m.",
		  "Therefore, this cave gained fame of having many zombies that never could leave for the beyond." );	 
	nextButton2();
	
	show("Ahhh...fantasmag�rico...",
		 "Ahhh ... spooky ...");
	endTalk();
	
	OnInit:
		SLN( PTBR, "Jovem Senhora" );
		end;
}

payon,134,211,4	script	Young Man#payon	59,{
	
	begin( strML( "Jovem Senhor", "Young Man" ),
		   0,
		   "Algumas criaturas de casca dura n�o sofrem danos dos ataques f�sicos.",
		   "Some hard-shelled creatures do not suffer damage from physical attacks." );
	nextButton2();
	
	show( "S� o poder ps�quico como a magia pode derrot�-las com facilidade.",
		  "Only psychic power like magic can easily defeat them." ); 
	endTalk();
	
	OnInit:
		SLN( PTBR, "Jovem Senhor" );
		end;
}

payon,173,82,0	script	Jovem Senhor#2payon	88,{
	
	begin( strML( "Jovem Senhor", "Young Man" ),
		   0,
		   "Eu ainda me lembro de uma hist�ria, que o meu av� contava; que os deuses o tenham.",
		   "I still remember a story, which my grandfather told." );
	nextButton2();
	
	show( "Diziam que havia um amuleto possu�do por um poder mal�gno que podia acordar os mortos do cemit�rio.",
		  "It was said that there was an amulet possessed by an evil power that could wake the dead from the graveyard." );	 
	nextButton2();
	
	show( "Bem, n�o sei se � verdade ou n�o, mas se for, eu me pergunto o que poderia acontecer se o meu av� fosse trazido do al�m por esse amuleto...",
		  "Well, I do not know if it's true or not, but if it is, I wonder what would happen if my grandfather were brought from beyond by this amulet..." ); 
	endTalk();
	
	OnInit:
		SLN( PTBR, "Jovem Senhor" );
		end;
}

payon,158,246,3	script	Guard#payon	708,3,3,{

	begin( strML( "Guarda", "Guard" ),
		   0,
		   "Esse � o pal�cio central de " + $S_CIDADEPAYON$ + ".",
		   "This is " + $S_CIDADEPAYON$ + "'s central palace." );
	endTalk();
	
	OnInit:
		SLN( PTBR, "Guarda" );
		end;
}

payon,249,156,1	script	Woman#payon	66,{
	
	begin( strML( "Mulher", "Woman" ),
		   0,
		   "Bem-vindo a " + $S_CIDADEPAYON$ + ". Deve ter sido muito duro passar pela floresta. Como foi a viagem?",
		   "Welcome to " + $S_CIDADEPAYON$ + ". It must have been very hard to go through the forest. How was the trip?" );
	nextButton2();
	
	show( "Quase n�o tem mais turistas em " + $S_CIDADEPAYON$ + ", devido aos monstros l� fora, est� virando uma cidade bem calma.",
		  "Almost no more tourists in " + $S_CIDADEPAYON$ + " due to the monsters out there is turning into a very quiet town." );
	nextButton2();
	
	show( "Gra�as a isso n�o tem muita coisa para fazer e eu passo o dia fofocando com as minhas amigas. hohoho!!!",
		  "Thanks to that there is not much to do and I spend the day gossiping with my friends. hohoho!!!" ); 
	endTalk();
	
	OnInit:
		SLN( PTBR, "Mulher" );
		end;
}

payon,246,158,5	script	Man#20payon	67,{
	
	begin( strML( "Homem", "Man" ),
		   0,
		   "Ai, pelo Deuses, l� vai ela de novo. Ela � uma fofoqueira sem cura.",
		   "Oh, by the Gods, there she goes again. She's a gossip without a cure." ); 
	nextButton2();
	
	show("Por favor, n�o acredite no que ela diz sobre outros alde�es. Nem todos n�s aqui em Payon temos uma boca t�o grande.",
		 "Please do not believe what she says about other villagers. Not all of us here in Payon have such a big mouth.");	 
	endTalk();
	
	OnInit:
		SLN( PTBR, "Homem" );
		end;
}

payon,210,110,1	script	Sincere Archer#payon	120,{

	if( Class == Job_Archer )
	{
		.@ptbr$ = "Um Arqueiro! Oh, voc�s s�o os caras! Voc�s s�o os melhores!";
		.@eng$  = "An Archer! Oh, you guys! You are the best!";
	}
	else
	{
		.@ptbr$ = "Ei, ei!! Eu me pergunto como � que os arqueiros est�pidos conseguem mirar sem acertar a pr�pria cabe�a.";
		.@eng$  = "Hey Hey!! I wonder how stupid archers can stare without hitting their own heads.";
	}

	begin( strML( "Arqueiro Alterado", "Sincere Archer" ), 0, .@ptbr$, .@eng$ );
	endTalk();
	
	OnInit:
		SLN( PTBR, "Arqueiro Alterado" );
		end;
}

payon,132,235,3	script	Monster Researcher#1	98,{
	
	begin( strML( "Pesquisador de Monstros", "Monster Researcher" ),
		   0,
		   "Prazer em conhec�-lo. Eu sou o pesquisador da Organiza��o de Pesquisa de Monstros de " + $S_RUNEMIDGARD$ + ".",
		   "Nice to meet you. I am the researcher of the " + $S_RUNEMIDGARD$ + " Monsters Research Organization." );   
	nextButton2();
	
	show( "Voc� tem alguma pergunta sobre os monstros de " + $S_RUNEMIDGARD$ + "?",
		  "Do you have any questions about " + $S_RUNEMIDGARD$ + "'s monsters?" );
		 
	nextButton2();
	
	.@menu$ = strML( "�ltimas Not�cias:Mortos-vivos?:Pesquisa de Monstros?",
	                 "Latest News:Undead Monsters?:Monster Search?" );	
	addOptionArray( .@menu$, BT_NORMAL);
	waitOption();
	clear();
	
	switch( BUTTON )
	{
		case 1:
			show( $S_CIDADEPAYON$ + " est� localizada bem dentro da floresta, alvo f�cil de ataques das tropas de monstros. Fora a perigosa caverna perto da cidade.",
				  $S_CIDADEPAYON$ + " is located well inside the forest, easy target of attacks of the troops of monsters. Besides the dangerous cave near the city." );
			nextButton2();
			
			show( "Especialmente na caverna, monstros morto-vivos s�o sempre vistos e o mundo acad�mico dos monstros est� muito atento a essa caverna.",
				  "Especially in the cave, undead monsters are always seen and the academic world of monsters is very attentive to this cave." );
			nextButton2();
			
			show( "Minha miss�o aqui � analisar as suas caracter�sticas.",
				  "My mission here is to analyze its characteristics." );
			break;
			
		case 2:
			show( "A caracter�stica mais formid�vel deles � sua origem.",
				  "Their most formidable characteristic is their origin." ); 
			nextButton2();
			
			show( "A maioria dos monstros era de pessoas de Payon que n�o conseguiram descansar em paz.",
				  "Most of the monsters were of Payon people who could not rest in peace." ); 
			nextButton2();
			
			show( "De algum modo, suas almas perdidas foram afetadas pela energia mal�gna do " + $S_SDT$[Language] + ".",
				  "Somehow, their lost souls were affected by the " + $S_SDT$[Language] + " evil energy." );
				 
			nextButton2();
			
			show( "� totalmente diferente dos outros monstros que sofreram muta��o de criaturas vivas.",
				  "It's totally different from the other monsters that have mutated from living creatures." );
			nextButton2();
			
			show( "Tenho certeza que conseguiremos um dia achar a cura.",
				  "I'm sure we'll someday find the cure." );
			break;
			
		case 3:
			show( "A Organiza��o de Pesquisa de Monstros foi formada para contra-atacar o aumento do n�mero de monstros nas nossas terras.",
				  "The Monster Research Organization was formed to counteract the increase in the number of monsters on our lands." );
			nextButton2();
			
			show( "Talentos de todo o mundo se uniram a essa organiza��o para estudar as origens dos monstros e aprender como derrot�-los.",
				  "Talents from around the world have joined this organization to study the origins of monsters and learn how to defeat them." );
			break;
	}
	endTalk();
	
	OnInit:
		SLN( PTBR, "Pesquisador de Monstros" );
		end;
}

payon_in01,180,7,2	script	Waitress#payon	90,{
	
	begin( strML( "Gar�onete", "Waitress" ),
		   0,
		   "As pessoas desse lugar parecem estar sempre ocupadas, v�m e v�o para ser arqueiros ou para comprar flechas e eu tenho de mofar nesse lugarzinho.",
		   "The people in this place always seem to be busy, they come and go to be archers or to buy arrows and I have to screw in this little place." );
	nextButton2();
	
	show( "Me sinto t�o melanc�lica e entediada...",
		  "I feel so melancholy and bored...");	 
	nextButton2();
	
	show( "Estou doente de tanto servir sopa. N�o aguento mais o cheiro de sopa, mas � t�o dif�cil me livrar desse cheiro... *suspiro*",
		  "I can not stand the smell of soup any more, but it's so hard to get rid of that smell ... * sigh *" );	 
	nextButton2();
	
	show("Quando ser� que vou encontrar uma pessoa realmente legal, gentil e que me tire daqui?",
		 "When will I find a really nice, kind person and get me out of here?"); 
	nextButton2();
	
	show( "Me desculpe, eu n�o devia ter dito isso para voc�. Agora estou agindo como uma est�pida.",
		  "I'm sorry, I should not have said that to you. Now I'm acting like a fool." ); 
	nextButton2();
	
	show( "Por favor, n�o ligue para mim.",
		  "Please do not bother yourself with me." );	 
	endTalk();
	
	OnInit:
		SLN( PTBR, "Gar�onete" );
		end;
}

payon_in03,96,116,3	script	Chief of the Guard#payon	708,3,3,{

	begin( strML( "Chefe da Guarda", "Chief of the Guard" ),
		   0,
		   "O que trouxe voc� aqui?",
		   "What brought you here?" );	
	
	nextButton2();
	
	show( "Eu posso ver que n�o � dessas bandas. Eu vou logo avisando para se comportar, enquanto estiver por aqui.",
		  "I can see that you are an outsider. I'll tell you to behave while you're here." );	 
	endTalk();
	
	OnInit:
		SLN( PTBR, "Chefe da Guarda" );
		end;
}

payon_in01,66,64,5	script	Archer Zakk#payon	88,{

	begin( strML( "Arqueiro Zakk", "Archer Zakk" ),
		   0,
		   "Estou t�o preocupado com um dos meus colegas. Ele fala tanta bobagem.",
		   "I'm so worried about one of my colleagues. He talks such nonsense." );
	nextButton2();
	
	show( "Ele � um arqueiro experiente, mas ele fala coisas como ''eis que'', ''pitanguinha'' e etc...",
		  "He is an experienced archer, but he speaks things like ''show me the way'', ''meme review'' and etc ..." );	 
	endTalk();
	
	OnInit:
		SLN( PTBR, "Arqueiro Zakk" );
		end;
}

payon_in01,47,59,2	script	Archer Wolt#payon	88,{	
		 
	begin( strML( "Arqueiro Wolt", "Archer Wolt" ),
		   0,
		   "Os arqueiros deveriam praticar tanto quando puderem. Ou nunca ser�o especialistas.",
		   "Archers should practice as much as they can. Or they will never be experts." );
	endTalk();
}

payon_in03,99,190,4	script	Boss#payon	107,2,2,{
	
	if( BaseLevel < 30 )
	{
		begin( strML( "Guarda", "Guarda" ),
			   0,
			   "Ei, ei! Tenha respeito com o chefe, seu herege!",
			   "Hey Hey! Have respect with the boss!" );
		endTalk();
	}
	else
	{
		begin( strML( "Guarda", "Guarda" ),
			   getnpcid(0),
			   "Ei! Eu disse...",
			   "Hey! I said..." );
		nextButton2();
		
		setName( "Chefe", "Boss");
		show   ( "Tudo bem, eu estou bem. Faz muito tempo que n�o falo com algu�m t�o jovem.",
			     "All right, I'm fine. It's been a long time since I've spoken to someone so young." );
		
		.@menu$ = strML( "Payon:Guardas:Monstros na Caverna:Arqueiros:Ca�adores",
		                 "Payon:Guards:Payon Dungeon:Archers:Hunters" );
						 
		addOptionArray( .@menu$, BT_NORMAL);
		waitOption();
		clear();
		
		switch( BUTTON )
		{
			case 1:
				show( $S_CIDADEPAYON$ + " � a cidade dos montanheses, que se viram sozinhos e sozinhos se bastam.",
					  $S_CIDADEPAYON$ + " is the city of the mountains, that if they saw alone and alone enough.");
				nextButton2();
				
				show( "Embora os nossos ancestrais n�o pudessem usufruir tanto dos benef�cios culturais, como os fazendeiros ou os alde�es, eles souberam tocar a vida sem precisar de ajuda.",
					  "Although our ancestors could not enjoy the cultural benefits so much as the farmers or the villagers, they knew how to touch life without needing help." );
				nextButton2();
				
				show( "Uma for�a de vontade ainda jovem os motivou a sobreviver �s for�as dos elementos. N�s, homens e mulheres de " + $S_CIDADEPAYON$ + ", aprendemos a ca�ar e a nos proteger dos perigos.",
					  "Still young willpower motivated them to survive the forces of the elements. We, the men and women of " + $S_CIDADEPAYON$ + ", have learned to hunt and protect ourselves from danger." );
				nextButton2();
				
				show( "Eu ouvi falar de jovens que temem campos ou calabou�os cheios de monstros. Mas, para n�s lutar contra monstros � parte de nossa vida.",
					  "I've heard of young men who fear fields or dungeons full of monsters. But fighting monsters is part of our lives." );
				break;
				
			case 2:
				show( "Quando eu era jovem, " + $S_CIDADEZERO$ + " enviou servos civis a " + $S_CIDADEPAYON$ + ".",
					  "When I was young, " + $S_CIDADEZERO$ + " sent civil servants to " + $S_CIDADEPAYON$ + "." );
				nextButton2();
				
				show( "Tropas reais, Funcion�rias Kafra, oficiais... no come�o, t�nhamos algumas discuss�es por causa das diferen�as de costumes.",
					  "Royal Troops, Kafra Officers, Officers ... At first, we had some discussions because of differences in customs." );
				nextButton2();
				
				show( "Por�m n�o posso negar que eles ajudaram a criar rapidamente um com�rcio externo com outros pa�ses.",
					  "But I can not deny that they helped quickly create foreign trade with other countries." );
				nextButton2();
				
				show("Hoje, mesmo quem tem origem fora de " + $S_CIDADEPAYON$ + " se considera um alde�o de " + $S_CIDADEPAYON$ + "... imposs�vel imaginar essa cena quando eu era jovem. Huh huh...",
					 "Today, even those who have originated outside of " + $S_CIDADEPAYON$ + " consider themselves a villager of " + $S_CIDADEPAYON$ + " ... impossible to imagine this scene when I was young. Huh huh..." );
				break;
				
			case 3:
				show( "A caverna ao norte � um lugar que eu costumava ir de vez em quando.",
					  "The cave to the north is a place I used to go from time to time." );
				nextButton2();
				
				show( "Os monstros de hoje s�o... diferentes dos monstros que costum�vamos combater.",
					  "The monsters of today are... different from the monsters we used to fight." );
				break;
				
			case 4:
				show( "N�s cortamos madeira da imensa floresta que cerca Payon.",
					  "We cut wood from the immense forest that surrounds Payon." );	
				nextButton2();
				
				show( "Um dos benef�cios � que n�s aprendemos a ser excelentes arqueiros. Voc� pode achar que a floresta atrapalha a pr�tica do arco e flecha, mas � bem o contr�rio.",
					  "One of the benefits is that we have learned to be excellent archers. You may find that the forest disrupts bow and arrow practice, but quite the opposite." );
				nextButton2();
				
				show( "A floresta ajuda a ocultar o arqueiro, bloqueia o inimigo e obriga-o a se aproximar para atacar.",
					  "The forest helps to hide the archer, blocks the enemy and forces him to approach to attack." );
				nextButton2();
				
				show( "Os arqueiros s�o muitos h�beis em atacar os inimigos a longa dist�ncia.",
					  "Archers are very skilled at attacking enemies at long range." );
				break;
				
			case 5:
				show( "Conforme as culturas estrangeiras se introduziram em " + $S_CIDADEPAYON$ + ", o nosso estilo de luta foi se alterando.",
					  "As foreign cultures introduced themselves to " + $S_CIDADEPAYON$ + ", our fighting style changed." );
				nextButton2();
				
				show( "Especialmente os compostos explosivos e as tecnologias tiveram um efeito not�vel no estilo de vida das pessoas. Parece que elas n�o est�o mais satisfeitas em ter s� arcos e flechas para lutar.",
					  "Especially explosive compounds and technologies have had a remarkable effect on people's lifestyles. It seems they are no longer content to have only bows and arrows to fight." );	
				nextButton2();
				
				show( "Foram desenvolvidas habilidades de armadilha o que facilitou a ca�a aos monstros, e assim o antigo chefe ganhou para si o nome de ca�ador.",
					  "Trap skills were developed which made it easier to hunt the monsters, and so the old chief earned him the name of a hunter." );	
				nextButton2();
				
				show( "Mesmo assim, fazer armadilha � uma habilidade perigosa e n�s n�o encorajamos os jovens a se tornar ca�adores.",
					  "Even so, making a trap is a dangerous skill and we do not encourage young people to become hunters." );
				break;
		}
		
		nextButton2();
		
		show( "Estou cansado... � melhor eu descansar.",
			  "I'm tired... I'd better rest." );		 
		nextButton2();
		
		show( "Bem, cuide-se.",
			  "Well, take care of yourself." );
	}
	endTalk();
	
	OnInit:
		SLN( PTBR, "Chefe" );
		end;
}

payon_in03,102,185,5	script	Guard#2payon	708,{
	
	if( BaseLevel < 30 )
	{
		begin( strML( "Guarda", "Guarda" ),
			   0,
			   "Ei, ei, ei, voc� n�o pode entrar a�.",
			   "Hey, hey, hey, you can not go in there." );
		nextButton2();
		
		show( "Saia, por favor.",
			  "Get out please." );
	}
	else
	{
		begin( strML( "Guarda", "Guarda" ),
			   0,
			   "Desculpe-me, mas n�o � permetido entrar.",
			   "Excuse me, but you're not allowed in." );
	}
	endTalk();
	
	OnInit:
		SLN( PTBR, "Guarda" );
		end;
}

pay_arche,77,131,2	script	Archer Josma#payon	88,{
	
	begin( strML( "Arqueiro Josma", "Archer Josma" ),
		   0,
		   "Payon! Que lugar ador�vel! Arcos maravilhosos! Excelentes arqueiros!",
		   "Payon! What a lovely place! Wonderful Arches! Great archers!" );
	nextButton2();
	
	show( "Ei, voc�! J� tinha ouvido falar da nossa fama?",
		  "Hey you! Ever heard of our fame?" );
	endTalk();
}
